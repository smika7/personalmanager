//
//  ViewController.swift
//  personalManager
//
//  Created by Muhammad Iqbal on 30/06/2019.
//  Copyright © 2019 Muhammad Iqbal. All rights reserved.
//

import UIKit
import GoogleSignIn

class LoginVC: UIViewController {
    
    

    // Outlets
    
    @IBOutlet weak var login_table_view: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.login_table_view.dataSource = self
        self.login_table_view.delegate = self
        self.login_table_view.sectionHeaderHeight = UITableView.automaticDimension
        self.login_table_view.estimatedSectionHeaderHeight = 1000
        self.login_table_view.register(UINib(nibName: "LoginTableHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "login_header_view")
        
        
    }

    
    
  
    
    
}

// MARK: TableView Delegates

extension LoginVC: UITableViewDelegate, UITableViewDataSource {
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let loginHeaderView = self.login_table_view.dequeueReusableHeaderFooterView(withIdentifier: "login_header_view") as! LoginTableHeader
        
        return loginHeaderView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if UIDevice.current.userInterfaceIdiom == .phone {
            
            return 620
        } else {
            
            return 1300
        }
    }
    
}






