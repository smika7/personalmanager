//
//  LoginTableHeader.swift
//  personalManager
//
//  Created by Muhammad Iqbal on 30/06/2019.
//  Copyright © 2019 Muhammad Iqbal. All rights reserved.
//

import UIKit
import GoogleSignIn

class LoginTableHeader: UITableViewHeaderFooterView {
    
   

  
    
    @IBOutlet weak var signIn_withGoogleButton: GIDSignInButton!
    
    
    // MARK: -IB Actions
    
    @IBAction func login_withGoogle_pressed(_ sender: Any) {
        print("google login pressed")
        
        GIDSignIn.sharedInstance().uiDelegate = self
       GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance().signIn()
        
    }
    
   
    
}

// MARK: - Google signin extension
extension LoginTableHeader: GIDSignInDelegate, GIDSignInUIDelegate {
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if user != nil {
            
            print("got next vc")
            guard let dashboardVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "dashboard_vc") as? DashboardVC else { return}
            let window  = UIApplication.shared.keyWindow
            var vc = window?.rootViewController
            
            while (vc?.presentedViewController != nil) {
                vc = vc?.presentedViewController
            }
            
            vc?.present(dashboardVC, animated: true, completion: nil)
        }
        
    }
    
  
    
    // Start Google OAuth2 Authentication
    func sign(_ signIn: GIDSignIn?, present viewController: UIViewController?) {
        
        // Showing OAuth2 authentication window
        if let aController = viewController {
            let window  = UIApplication.shared.keyWindow
            var vc = window?.rootViewController
            
            while (vc?.presentedViewController != nil) {
                vc = vc?.presentedViewController
            }
            
            vc?.present(aController, animated: true, completion: nil)
        }
    }
    
    // After Google OAuth2 authentication
    func sign(_ signIn: GIDSignIn?, dismiss viewController: UIViewController?) {
        let window  = UIApplication.shared.keyWindow
        var vc = window?.rootViewController
        
        while (vc?.presentedViewController != nil) {
            vc = vc?.presentedViewController
        }
        
        // Close OAuth2 authentication window
        vc?.dismiss(animated: true) {() -> Void in }
        
    }
    
    
    
}
